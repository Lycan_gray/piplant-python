# PiPlant-python v1.1.0

PiPlant Python script for the Raspberry Pi 3B+



<strong>Usage</strong><br>
Set `pi_id` variable to random number.<br>
That's all :)<br>

<strong>Command</strong><br>
`python3 plant.py`



Made by Peter Boersma
