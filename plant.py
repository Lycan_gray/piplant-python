#!/usr/bin/python

# Modules
import RPi.GPIO as GPIO
import Adafruit_DHT
import vcgencmd
import requests

#--------------------------------------------------
# Set Variables
sensor = Adafruit_DHT.DHT11
callbackUrl = 'https://sensor.lycan-media.nl/api/store'
pi_id = 1
plaats = "Krommenie"


#--------------------------------------------------
# Helper Functions

def setData(pi_id, pi_gpio, sensor_name, sensor_type, value):
    data = {'id' : 0, 'pi_id' : pi_id, 'pi_gpio' : pi_gpio, 'sensor_name' : sensor_name, 'sensor_type' : sensor_type, 'value' : value }
    return data

def postData(callbackUrl, data):
    response = requests.post(callbackUrl, data)
    return response


#--------------------------------------------------
# Humidity and Temperature Sensor

def getHumTemp(pi_id, sensor, pin, callbackUrl):
    sensor_name = "DHT11"
    callbackUrl = callbackUrl + "/log"
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)


    humData = setData(pi_id, pin, sensor_name, "humidity", humidity)
    tempData = setData(pi_id, pin, sensor_name, "temperature", temperature)
    
    return postData(callbackUrl, humData),postData(callbackUrl, tempData)

print(getHumTemp(pi_id, sensor, 23, callbackUrl))
#--------------------------------------------------
# Realtime Temp & Hum

def getRTTempHum(pi_id, callbackUrl, plaats):
    callbackUrl = callbackUrl + "/logrt"
    reqlink = "http://weerlive.nl/api/json-data-10min.php?key=80da7c6268&locatie=" + plaats

    r = requests.get(reqlink).json()
    temp = r['liveweer'][0]['temp']
    hum = r['liveweer'][0]['lv']
    plaats = r['liveweer'][0]['plaats']

    rtweather = {'0' : 0, 'pi_id' : pi_id, 'plaats' : plaats, 'temp' : temp, 'hum': hum }
    return postData(callbackUrl, rtweather)


print(getRTTempHum(pi_id, callbackUrl, plaats))


#--------------------------------------------------
def getWaterStatus(pi_id, sensor_name, pin, callbackUrl):
    callbackUrl = callbackUrl + "/log"
    
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.IN)
    def callback(pin):
        if GPIO.input(pin):
            return False
        else:
            return True

    hasWater = callback(pin)
    moistData = setData(pi_id, pin, sensor_name, "soil_moisture", hasWater)
    return postData(callbackUrl, moistData)

print(getWaterStatus(pi_id, "Venus Flytrap", 25, callbackUrl))
print(getWaterStatus(pi_id, "Carolina Reaper", 27, callbackUrl))

#--------------------------------------------------
# Pi Cpu Temperature

def getCpuTemp(pi_id, callbackUrl):
    callbackUrl = callbackUrl + "/cpuLog"
    cpuTemp = vcgencmd.measure_temp()
    piData = {'id' : 0, 'pi_id' : pi_id, 'cpu_temp' : cpuTemp }
    return postData(callbackUrl, piData)

print(getCpuTemp(pi_id, callbackUrl))
